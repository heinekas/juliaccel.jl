# JuliAccel.jl

## PAM 1 HS 2022

JuliAccel.jl is a package developped for the Particule Accelerator Physics and Modelling I course given at ETH Zürich by Dr. sc. math. Andreas Adelmann and Sichen Li during the Fall Semester of 2022.
It offers tools to model some simple Particle Accelerator Beamline Elements at linear order.

## Installation

To install this package, clone this repository on your local machine in the wanted directory via 
```
$ cd path/to/wanted/dir
$ git clone https://gitlab.ethz.ch/heinekas/juliaccel.jl.git
```
Then build the JuliAccel package by openning your Julia REPL and rinnung the following commands
```
julia> cd("path/to/juilaccel.jl/dir")
```
Open the Pkg REPL by pressing `]`. Next run
```
(@v1.11) pkg> activate juliAccel.jl

(JuliAccel) pkg> add Pkg

(JuliAccel) pkg> instantiate

```

## Activation

Anytime you wish to use the JuliAccel package, you will have to run the following commands in the Julia REPL
```
julia> cd("path/to/wokring/dir")

(@v1.11) pkg> activate /path/tojuliAccel.jl 
```
To then exit the activated JuliAccel envirronment, simply run `activate` in your pkg REPL.

If you wish to use this package within a Jupyter Notebook, add and run, at the top of your notebook, the following cell
```
using Pkg

Pkg.activate("../relative/path/to/juliAccel.jl")
```
Don't forget to import the package on top of your code via
```
using JuliAccel
```

## Documentation

In case you are wondering what a function/struct does, Documentation is available by running a cell
```
?<function name>
```

One can also refere to the [manual](JuliAccel.jl-manual.pdf)