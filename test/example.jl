using Pkg
Pkg.activate("JuliAccel.jl")

using JuliAccel
import JuliAccel.gen_map    # Necessary to be able to expand its methods


## Define particle beam
ekin = 100e6  # eV
particle = Proton()
print(particle)

beam = Beam(particle, ekin)


## Define particle Bunch
μ = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
Σ = [16.0 0.0 0.0 0.0 0.0 0.0;
     0.0 1.0 0.0 0.0 0.0 0.0;
     0.0 0.0 16.0 0.0 0.0 0.0;
     0.0 0.0 0.0 1.0 0.0 0.0;
     0.0 0.0 0.0 0.0 0.709 0.0;
     0.0 0.0 0.0 0.0 0.0 0.0981]

D = Gaussian(μ, Σ)
N = 1000

Γ = genDistrib(D, N)
Γi = deepcopy(Γ)    # Else gamma changes!

bunch = Bunch(Γ, beam)


## Generate map for a Drift
l = 10.0
Dr = Drift(l)

function gen_map(Dr::Drift, B::Beam)
    γ = get_gamma(B)
    println(γ)
    β0 = get_beta(γ)

    f = Dr.length / (β0 * γ)^2

    R = [1 Dr.length 0 0 0 0
         0 1 0 0 0 0
         0 0 1 Dr.length 0 0
         0 0 0 1 0 0
         0 0 0 0 1 f
         0 0 0 0 0 1]

    return R
end

M = gen_map(Dr, beam)

Γf = evolve(M, bunch)


## Plot output
using LaTeXStrings
using Plots

x_i = Γi[1,:]
px_i = Γi[2,:]

x_f = Γf[1,:]
px_f = Γf[2,:]

plot(x_i, px_i, seriestype=:scatter, label=L"\xi_i")
plot!(x_f, px_f, seriestype=:scatter, label=L"\xi_f")
xaxis!(L"x")
yaxis!(L"p_x")
title!("Horizontal phase space")

# display(plot)

savefig("drift_ps.pdf")