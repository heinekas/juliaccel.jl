"""Abstract type for distributions."""
abstract type Distrib end

"""
$(SIGNATURES)

Generates `N` datapoints following distribution `D`.
"""
function gen_distribution(D::Distrib, N::Integer) end

"""
$(TYPEDEF)

Gaussian Distribution instance.

# Fields

$(FIELDS)
"""
struct Gaussian{Tm<:Vector{<:Real}, Tc<:Matrix{<:Real}} <: Distrib
    """Mean"""
    μ::Tm
    """Covariance Matrix"""
    Σ::Tc
end
gen_distribution(GD::Gaussian, N::Int) = rand(MvNormal(GD.μ, GD.Σ), N)
