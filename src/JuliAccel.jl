module JuliAccel

using Reexport

@reexport using TaylorSeries
using TaylorSeries: gradient, jacobian
@reexport using LinearAlgebra

using ProgressBars
using DocStringExtensions

include("mad_x_parser.jl")
@reexport using .MADXParser

export extract_model_data, ModelData
include("model_parser.jl")

export @set_problem, tps_expansion!
include("utils.jl")

export Constants
include("constants.jl")

export Proton, Electron
include("particles.jl")

export get_gamma, get_beta, get_lorentz_factors
include("physics.jl")

using Distributions, Random
Random.seed!(42)
export Distrib, gen_distribution, Gaussian
include("distributions.jl")


export lie_bracket, lie_transform
include("lie.jl")

export dragt_finn_factorisation
include("symplectic.jl")

export jacobian_det, concatenate
include("tools.jl")

export gen_maps
include("gen_maps.jl")

end # module JuliAccel
