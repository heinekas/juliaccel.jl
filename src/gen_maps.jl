struct ParsingError <: Exception
    obj
    ParsingError(obj) = new(obj)
end
Base.showerror(io::IO, e::ParsingError) = print(io, e.obj)

"""
$(SIGNATURES)

Provided well formated MAD X and model data files, `mad` and `prob_def` respectively, computes the transfer polynomials for each element of the beamline and returns an ordered array containing these.
The argument `symplectic` allows the user to explicitly Dragt-Finn factorise each map (`symplectic=true`) or not (`symplectic=false`).
    
# Examples
```jldoctest

julia> mad_x_file = "path/to/mad_x/file.mad"

julia> prob_set_file = "path/to/problem/data/file.japd"

julia> Ms = gen_maps(mad_x_file, prob_set_file, symplectic=true)

julia> typeof(Ms)
 Vector{Vector{TaylorN{Flaot64}}}
```
"""
function gen_maps(mad::String, prob_def::String; symplectic=true)::Array{Vector{TaylorN{Float64}}}

    mad_parsed = parse_madx(mad)

    prob_set = extract_model_data(prob_def)

    eval(:(const γ0, β0 = get_lorentz_factors($mad_parsed)))

    Γ = eval(:(@set_problem(
            $(prob_set.variables),
            $(prob_set.truncation_orders["trunc_order"]),
            $(prob_set.truncation_orders["exp_order"]),
            $(prob_set.truncation_orders["order"])
        )))

    maps = Array{Vector{TaylorN{Float64}}}(undef, length(mad_parsed.lattice))

    for elem_idx in ProgressBar(1:size(mad_parsed.lattice)[1])

        element = mad_parsed.lattice[elem_idx]
        
        found = false
        for ham in prob_set.hamiltonians

            if ham["type"] == element["type"]

                # println("Element: $(element["name"])")
                found = true
                
                for arg in ham["args"]
                    if string(arg) != ""
                        eval(Meta.parse(arg*"="*string(element[arg])))
                    end
                end

                eval(Meta.parse("H="*ham["expr"]))

                H_polynom = tps_expansion!(H)

                l = element["l"]

                M = lie_transform(-l*H_polynom, Γ)

                if symplectic

                    M = dragt_finn_factorisation(M)
                end

                maps[elem_idx] = M
            end
        end

        if ! found
            err = "hamiltonian for element "*element["type"]*" was not defined"
            throw(ParsingError(err))
        end
    end

    return maps
end
