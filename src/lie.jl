"""
$(SIGNATURES)

Computes the Lie bracket of two `TaylorN{Float64}` instances.
"""
function lie_bracket(f::T, g::T)::T where T<:TaylorN{Float64}

    lie = Array{TaylorN{Float64}}(undef, Γdim)

    for i in 1:Γdim

        # println(i)

        i_bar = isodd(i) ? i+1 : i-1
        coeff = (1 - 2*iseven(i))

        df = differentiate(f, i)
        dg = differentiate(g, i_bar)

        lie[i] = coeff * df*dg
    end

    return sum(lie)

end


function _exponential_terms!(a::Vector{T}, gen::T)::Vector{T} where T<:TaylorN{Float64}

    for i in 2:size(a)[1]

        a[i] = lie_bracket(gen, a[i-1])
    end

    return a
end

function _taylor_coeffs!(c::Vector{Float64})::Vector{Float64}

    for i = eachindex(c)

        c[i] = i<21 ? 1 / factorial(i-1) : 1/factorial(big(i-1))
    end

    return c
end


"""
$(SIGNATURES)

Computes the action of the Lie transform associated to the first argument `f`, truncated at order `n`, on `g`.
If `g` is a vector, the trnansform is broadcast over the entries of the vector.
The truncation order `n` takes default value `n2` for the exponential, defined using the `@set_problem` macro.

# Examples
```jldoctest
julia> Γ = @set_problem("q p", 3, 4, 9)
2-element Vector{TaylorN{Float64}}:
  1.0 q + 𝒪(‖x‖¹¹)
  1.0 p + 𝒪(‖x‖¹¹)

julia> H = (q^2 + p^2) / 2
 0.5 q² + 0.5 p² + 𝒪(‖x‖¹¹)

julia> lie_transform(H, Γ)
2-element Vector{TaylorN{Float64}}:
  0.5416666666666666 q - 0.8333333333333334 p + 𝒪(‖x‖¹¹)
  0.8333333333333334 q + 0.5416666666666666 p + 𝒪(‖x‖¹¹)
```
"""
function lie_transform(f::T, g::T; n::Int64=exp_order)::T where T<:TaylorN{Float64}

    expo    = Vector{TaylorN{Float64}}(undef, n+1)
    expo[1] = g
    _exponential_terms!(expo, f)

    coeffs = Vector{Float64}(undef, n+1)
    _taylor_coeffs!(coeffs)

    return  dot(coeffs, expo)
end


function _exponential_terms!(a::Matrix{T}, gen::T)::Matrix{T} where T<: TaylorN{Float64}

    shape = size(a)

    for i in 2:shape[2]
        for j in 1:shape[1]

            a[j,i] = lie_bracket(gen, a[j,i-1])
        end
    end

    return a
end

function lie_transform(f::T, g::Vector{T}; n::Int64=exp_order)::Vector{T} where T<:TaylorN{Float64}

    expo      = Matrix{TaylorN{Float64}}(undef, Γdim, n+1)
    expo[:,1] = copy(g)
    _exponential_terms!(expo, f)

    coeffs = Vector{Float64}(undef, n+1)
    _taylor_coeffs!(coeffs)

    return [sum(expo[i,:] .* coeffs) for i in 1:Γdim]
end