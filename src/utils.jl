"""
A macro for defining the TPSE data: variables and truncation orders.

```julia
@set_problem("q p", n1, n2, m_max)

H = (q^2 + p^2) / 2
```

In the above code snipet
    - `n1` is the truncation order used by `tps_expansion!`;
    - `n2` is the truncation order for exponential expansions used by `lie_transform`;
    - `n_max` is the truncation order for any symbolic expression.
"""
macro set_problem(Γ::String, trunc_order::Int64, exp_order::Int64, order::Int64)

    @assert trunc_order ≤ order

    Γexpr = :(global Γcoord::Vector{TaylorN{Float64}} = set_variables($Γ, order=$(order)))
    eval(Γexpr)

    Γstr = replace(Γ, " "=>", ") * " = set_variables("*'"'*"$Γ"*'"'*", order=$(order))"

    eval(:(global trunc_order::Int64 = $trunc_order))
    eval(:(global order::Int64 = $order))
    eval(:(global exp_order::Int64 = $exp_order))
    eval(:(global Γdim::Int64 = length($Γcoord)))

    return esc(Meta.parse(Γstr))
end


"""
$(SIGNATURES)

Computes the truncated power series expansion of `f` at order `n1` defined via the `@set_problem` macro.
Modifies the input function `f`.

# Examples
```jldoctest
julia> @set_variables("q p", 3, 5, 10)

julia> f = q + q*p - q*p^3 + 2q^2*p^3
 1.0 q + 1.0 q p - 1.0 q p³ + 2.0 q² p³  + 𝒪(‖x‖¹¹)

julia> tps_expansion!(f)

julia> f
 1.0 q + 1.0 q p - 1.0 q p³ + 𝒪(‖x‖¹¹)
```
"""
function tps_expansion!(f::TaylorN{Float64})::TaylorN{Float64}

    @assert trunc_order::Int64 ≤ get_order()

    for k in trunc_order+2::Int64:get_order()+1

        f.coeffs[k] = 0.0
    end

    return f
end
