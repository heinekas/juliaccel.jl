abstract type Particle end

"""
$(TYPEDEF)

Proton instance.

# Fields
$(FIELDS)
"""
struct Proton <: Particle
    """Particle mass"""
    mass::Real
    """Particle charge"""
    charge::Real
    """Particle name"""
    pname::String
    Proton() = new(Constants["pmass"], 1.0, "proton")
end

"""
$(TYPEDEF)

Electron instance.

# Fields
$(FIELDS)
"""
struct Electron <: Particle
    """Particle mass"""
    mass::Real
    """Particle charge"""
    charge::Real
    """Particle name"""
    pname::String
    Electron() = new(Constants["emass"], -1.0, "electron")
end


function Base.print(io::IO, P::Particle)
    print(io, "        Particle:    ")
    println(io, P.pname)
    print(io, "            * charge:    ")
    print(io, P.charge)
    println(io, " C")
    print(io, "            * mass:      ")
    print(io, P.mass)
    println(io, " eV/c^2")
end
Base.show(io::IO, P::Particle) = print(io, P)


particle_labels = Dict("proton"=>Proton(), "electron"=>Electron())
