"""
$(SIGNATURES)

Computes the Jacobian of the set of multivariate polynomials `M` and computes its value at coordinates `coords`.
If no coordinates `coords` are given, the Jacobian will be evaluated at 0.
If `coords` is a Matrix, the value of the Jacobian will be averaged over the columns of `coords`.
"""
function jacobian_det(M::Vector{TaylorN{Float64}}, coords::Vector{Float64})::Float64

    jac = jacobian(M, coords)
    jac_det = abs(det(jac))

    return jac_det
end

function jacobian_det(M::Vector{TaylorN{Float64}})::Float64

    jac = jacobian(M)
    jac_det = abs(det(jac))

    return jac_det
end


function jacobian_det(M::Vector{TaylorN{Float64}}, coords::Matrix{Float64})::Float64

    avg = 0.0

    for n in 1:size(coords)[2]
        jac_det = jacobian_det(M, coords[:,n])
        avg += jac_det
    end

    avg /= size(coords)[2]
    return avg
end


"""
$(SIGNATURES)

This function concatenates Lie trnasfer maps: `M∘N`.
"""
function concatenate(M::Vector{T}, N::Vector{T})::Vector{T} where T<:TaylorN{Float64}

    dim = size(M)[1]
    M_concat = Vector{TaylorN{Float64}}(undef, dim)

    for i in 1:dim

        M_concat[i] = evaluate(M[i], N)
    end

    return M_concat
end