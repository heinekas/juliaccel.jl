"""
$(SIGNATURES)

Computes the Lorentz factor γ from the kinetic energy `ekin` of the beam and particle mass `mass`.
"""
function get_gamma(ekin::Real, mass::Real)
    return (ekin / mass) + 1
end

"""
$(SIGNATURES)

Computes the velocity ratio β from the Lorentz factor γ.
"""
function get_beta(γ::Real)
    return sqrt(1- 1/γ^2)
end

"""
$(SIGNATURES)

Computes the γ and β factors from the beam data contained in a `MAD_X` instance obtained from parsing a MAD X file
See the `MADXParser` documentation.

# Example
```jldoctest

julia> mad_x_file = "path/to/MADX/file.mad"

julia> mad_x = parse_madx(mad_x_file)

julia> γ, β = get_lorentz_factors(mad_x)

julia> γ, β
(5.866462141020892, 0.9853645122468465)
```
"""
function get_lorentz_factors(mad::MAD_X)

    beam = mad.beam

    particle = particle_labels[beam["particle"]]
    ekin = beam["energy"]

    γ = get_gamma(ekin, particle.mass)
    β = get_beta(γ)

    return γ, β
end