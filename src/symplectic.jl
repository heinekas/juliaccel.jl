"""
$(SIGNATURES)

This function computes the generators of the a set of polynomials `M`, and concatenates their associated Lie transforms.
It returns a set of polynomials corresponding to the Lie transformations obtained with Dragt-Finn factorisation of `M`.

# Examples
```jldoctest
julia> H = (q^2 + p^2) / 2
 0.5 q² + 0.5 p² + 𝒪(‖x‖¹¹)

julia> lie_transform(H, Γ) |> dragt_finn_factorisation
2-element Vector{TaylorN{Float64}}:
  0.6728716563786007 q - 0.7368827160493827 p + 𝒪(‖x‖¹¹)
  0.7368827160493827 q + 0.6728716563786007 p + 𝒪(‖x‖¹¹)
```
"""
function dragt_finn_factorisation(M::Vector{T})::Vector{T} where T<:TaylorN{Float64}

    k_max = trunc_order

    # Storage for generators
    generators = Array{T}(undef, k_max-1)

    # Isolate Constant and linear terms
    shift  = constant_term.(M)
    M_tilde = M .- shift

    # Iterate over orders
    for k in 2:k_max

        if k > get_order()
            generators[k_max-k+1] = TaylorN(0.0, order+1)

        else
            # Identify :L_k: with M_tilde
            Lk = integrate(-M_tilde[1], 2)
            for i in 2:size(M_tilde)[1]

                i_bar = isodd(i) ? i+1 : i-1
                coeff = (1 - 2*isodd(i))

                Lk += integrate(coeff*M_tilde[i], i_bar)
            end

            p = x -> x!=0
            mask = count.(p, TaylorSeries.coeff_table[k+1])

            Lk.coeffs[k+1].coeffs ./= mask
            Lk = TaylorN(Lk.coeffs[k+1], order)

            # Save Lk
            generators[k_max-k+1] = Lk

            # Correct for higher order terms
            M_tilde = lie_transform(-Lk, M_tilde)
        end
    end

    ξ = copy(Γcoord)
    for h in 1:size(generators)[1]
        ξ = lie_transform(generators[h], ξ)
    end
    ξ .= ξ + shift

    return ξ
end
