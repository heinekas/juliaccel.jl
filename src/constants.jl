using PhysicalConstants.CODATA2018
using NaturallyUnitful

Constants = Dict(
    "clight"   => ustrip(+ SpeedOfLightInVacuum),           # m / s
    "echarge"  => ustrip(+ ElementaryCharge),               # C
    "pmass"    => ustrip(natural(+ ProtonMass)),            # eV / c^2
    "emass"    => ustrip(natural(+ ElectronMass)),          # eV / c^2
    "epsilon0" => ustrip(+ VacuumElectricPermittivity)      # F / m
)
