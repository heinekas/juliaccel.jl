"""
$(TYPEDEF)

Output of the model data file parsing.

# Fields
$(FIELDS)
"""
struct ModelData
    """Variables symbol"""
    variables::String
    """Truncation orders"""
    truncation_orders::Dict{String, Integer}
    """Hamiltonian formulas"""
    hamiltonians::Vector{Dict{String, Any}}
end


abstract type MADXParserError <: Exception end

struct ModelInputError <: MADXParserError
    obj
    ModelInputError(obj) = new(obj)
end
Base.showerror(io::IO, e::ModelInputError) = print(io, e.obj)



_var_pattern = r"variables=\((.*)\);"
_vars = ""
_order_pattern = r"orders,trunc_order=(.*),exp_order=(.*),order=(.*);"
_orders = Dict("trunc_order"=>0, "order"=>0, "exp_order"=>0)

_ham_pattern = r"hamiltonian,type=(.*),args=\(+(.*)\),expr=(.*);"
_hamiltonian = Dict("args"=>:, "expr"=>:, "type"=>"")


"""
$(SIGNATURES)

Parses the precised map generation data, i.e. vaiables, truncation orders and Hamiltonian formulas. 
Returns a ModelData instance.

# Example
```jldoctest

julia> file = "path/to/model/data.japd"

julia> extract_model_data(hamiltonians)
```
"""
function extract_model_data(file::String)

    if ! occursin(".japd", file)
        err = "File " * file * " not a .japd type file."
        throw(ModelInputError(err))
    end

    hamiltonians = []

    open(file) do f
        nLine = 0

        while ! eof(f)

            nLine += 1
            line = readline(f)

            stripped = replace(line, " " => "")
            
            if isempty(stripped)
                # Do nothing
            elseif string(stripped[1]) == "!"
                # Do nothing

            elseif occursin("variables", stripped)
                obj = collect(match(_var_pattern, stripped))
                global _vars = replace(string(obj[1]), ","=>" ")
            
            elseif occursin("order", stripped)
                obj = collect(match(_order_pattern, stripped))
                _orders["trunc_order"] = eval(Meta.parse(string(obj[1])))
                _orders["exp_order"] = eval(Meta.parse(string(obj[2])))
                _orders["order"] = eval(Meta.parse(string(obj[3])))
            
            elseif occursin("expr", stripped)
                obj = collect(match(_ham_pattern, stripped))
                _hamiltonian["type"] = obj[1]
                
                args = string.(split(obj[2], ","))
                _hamiltonian["args"] = args

                _hamiltonian["expr"] = obj[3]

                push!(hamiltonians, copy(_hamiltonian))
            end
        end
    end

    return ModelData(_vars, _orders, hamiltonians)
end


